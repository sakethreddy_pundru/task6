//
//  ViewController.m
//  Task6
//
//  Created by admin on 26/09/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UILabel *label;
UITextField *textField1;
UITextField *textField2;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UILabel *bgColor = [[UILabel alloc]init];
    bgColor.backgroundColor=[UIColor colorWithRed:203/255.0f green:203/255.0f blue:203/255.0f alpha:1.0];
    bgColor.frame=self.view.frame;
    [self.view addSubview:bgColor];
    
    label = [[UILabel alloc]initWithFrame:CGRectMake(150,0,250,200)];
    label.textColor = [UIColor blackColor];
    [self.view addSubview:label];
    
    textField1= [[UITextField alloc]initWithFrame:CGRectMake(75, 120, 200, 50)];
    textField1.backgroundColor = [UIColor lightTextColor];
    textField1.textColor = [UIColor blackColor];
    textField1.delegate = self;
    textField1.placeholder = @"TextField1";
    [self.view addSubview:textField1];
    
    textField2 = [[UITextField alloc]initWithFrame:CGRectMake(75, 200, 200, 50)];
    textField2.backgroundColor = [UIColor lightTextColor];
    textField2.textColor = [UIColor blackColor];
    textField2.delegate = self;
    textField2.placeholder = @"TextField2";
    [self.view addSubview:textField2];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    label.text= textField.text;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
